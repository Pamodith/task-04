package Unittesting;

import java.util.*;

public class Unittest {
  private static List<String> Trial;
  private static int Mcheck = 0;
  private static int passed_trial = 0;
  private static int failed_trial = 0;

  private static void Tothereport(String cont) {
    if (Trial == null) {
      Trial = new LinkedList<String>();
    }
    Trial.add(String.format("%04d: %s", Mcheck++, cont));
  }
  
  public static void checkEquals(double value_1, double value_2) {
	  if (value_1 == value_2) {
	  Tothereport(String.format(" %.2f == %.2f", value_1, value_2));
	  passed_trial++;
	  } else {
	  Tothereport(String.format("* %.2f == %.2f", value_1, value_2));
	  failed_trial++;
	  }
	  }
  
  public static void checkNotEquals(double value_1, double value_2) {
	  if (value_1 != value_2) {
	  Tothereport(String.format(" %.2f != %.2f", value_1, value_2));
	  passed_trial++;
	  } else {
	  Tothereport(String.format("* %.2f != %.2f", value_1, value_2));
	  failed_trial++;
	  }
	  }



  
  public static void checkEquals(int value_1, int value_2) {
    if (value_1 == value_2) {
      Tothereport(String.format("  %d == %d", value_1, value_2));
      passed_trial++;
    } else {
      Tothereport(String.format("* %d == %d", value_1, value_2));
      failed_trial++;
    }
  }

  public static void TrialtringCharAt(String value_1, String value_2) {
	  try {
	        char n1 = value_1.charAt(5);
	        char n2 = value_2.charAt(5);
	        if (value_1.length() >= 5 && value_2.length() >= 5) {
	            if (Character.isDigit(n1) || Character.isDigit(n2)) {
	                Tothereport(String.format("Contain letters and numbers are = %s and %s ", value_1, value_2));
	                failed_trial++;
	            } else if (value_1.charAt(5) == n1 && value_2.charAt(5) == n2) {
	                Tothereport(String.format("Contain the expected characters are = %s and %s ", value_1, value_2));
	                passed_trial++;
	            } else {
	                Tothereport(String.format("Do not contain the expected characters are =%s and %s ", value_1, value_2));
	                failed_trial++;
	            }
	        } 
	    } catch (StringIndexOutOfBoundsException e) {
	    	Tothereport(String.format("Both strings Should be > 5 letters"));
            failed_trial++;
	    }
	}

  public static void checkNotEquals(int value_1, int value_2) {
    if (value_1 != value_2) {
      Tothereport(String.format("  %d != %d", value_1, value_2));
      passed_trial++;
    } else {
      Tothereport(String.format("* %d != %d", value_1, value_2));
      failed_trial++;
    }
  }

  public static void report() {
    System.out.printf("%d Trial passed\n", passed_trial);
    System.out.printf("%d Trial failed\n", failed_trial);
    System.out.println();
    
    for (String check : Trial) {
      System.out.println(check);
    }
  }
}
