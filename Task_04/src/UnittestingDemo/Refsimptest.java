package UnittestingDemo;
import static Unittesting.Unittest.*;

public class Refsimptest {

  void checkConstructorAndAccess(){
	  Refsimp obj = new Refsimp(37.2, 35.2);
    checkEquals(obj.getValue_1(), 7.23);
    checkEquals(obj.getValue_2(), 4);
    checkNotEquals(obj.getValue_2(), 6.5);    
    checkNotEquals(obj.getValue_2(), 5);    
  }
  
  void checkStringCharAtValues(){
	  Refsimp obj = new Refsimp();
	  TrialtringCharAt("Alex1998", "Mark42");
	  obj = new Refsimp(25, 0);
		  TrialtringCharAt("Pamodith", "Maduwantha");
		  obj = new Refsimp(25, 0);
		  TrialtringCharAt("a", "b");
		  
	}

  void checkSquareA(){
	  Refsimp obj = new Refsimp(3, 4);
	  obj.sqrtValue_1();
    checkEquals(obj.getValue_1(), 9);
  }

  public static void main(String[] args) {
	  Refsimptest obj2 = new Refsimptest();
	  obj2.checkConstructorAndAccess();
	  obj2.checkSquareA();
	  obj2.checkStringCharAtValues();
    report();
  }
}
