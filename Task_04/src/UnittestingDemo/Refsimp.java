package UnittestingDemo;


public class Refsimp {
public double Value_1 = 30.7;
private double Value_2 = 42.8;
public Refsimp() {
}
public Refsimp(double Value_1, double Value_2) {
this.Value_1 = Value_1;
this.Value_2 = Value_2;
}
public void sqrtValue_1() {
this.Value_1 = Math.sqrt(this.Value_1);
}
private void sqrtValue_2() {
this.Value_2 = Math.sqrt(this.Value_2);
}
public double getValue_1() {
return Value_1;
}
private void setValue_1(double Value_1) {
this.Value_1 = Value_1;
}
public double getValue_2() {
return Value_2;

}
public void setValue_2(double b) {
this.Value_2 = b;
}
public String toString() {
return String.format("Value_1 : %.2f\nValue_2 : %.2f", Value_1, Value_2);
}
}